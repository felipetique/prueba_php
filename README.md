# suplosBackEnd

Prueba suplos desarrollador backend.

## Requeriments ##

Requeriments  | Version
------------- | -------------
PHP  | >= 7.2
Mysql  |  Ver 15.1 Distrib 10.4.14-MariaDB

## Deploy ##

Colocar la carpeta en var/www/html/ en linux.

o

Colocar la carpeta en el htdocs de xammp.

Importar la DB que se encuentra en la carpeta api/db.