<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// initialize object
$data = json_decode(file_get_contents("../assets/data-1.json"), true);

// set response code - 200 OK
http_response_code(200);

// show products data in json format
echo json_encode($data);
//$product = new Product()