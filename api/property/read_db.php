<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
include_once '../config/database.php';
include_once '../objects/property.php';
  
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$property = new Property($db);

// query propertys
$stmt = $property->read();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // propertys array
    $propertys_arr=array();
    $propertys_arr["records"]=array();
  
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
  
        $property_item=array(
            "id" => $id,
            "ciudad" => $ciudad,
            "direccion" => $direccion,
            "telefono" => $telefono,
            "codigo_postal" => $codigo_postal,
            "tipo" => $tipo,
            "precio" => $precio
        );
  
        array_push($propertys_arr["records"], $property_item);
    }
  
    // set response code - 200 OK
    http_response_code(200);
  
    // show propertys data in json format
    echo json_encode($propertys_arr);
}
  
// no propertys found will be here