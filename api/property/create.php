<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate property object
include_once '../objects/property.php';
 
$database = new Database();
$db = $database->getConnection();
$property = new Property($db);
$body = json_decode(file_get_contents("php://input"));
// get posted data
$data_json = json_decode(file_get_contents("../assets/data-1.json"), true);
$pre = array_filter($data_json,function ($var) use ($body) {return ($var["Id"] == $body->Id);});
$data = reset($pre);
// make sure data is not empty
if(
    !empty($data["Ciudad"]) &&
    !empty($data["Direccion"]) &&
    !empty($data["Telefono"]) &&
    !empty($data["Codigo_Postal"]) &&
    !empty($data["Tipo"]) &&
    !empty($data["Precio"])
){
 
    // set property property values
    $property->ciudad = $data["Ciudad"];
    $property->direccion = $data["Direccion"];
    $property->telefono = $data["Telefono"];
    $property->codigo_postal = $data["Codigo_Postal"];
    $property->tipo = $data["Tipo"];
    $property->precio = $data["Precio"];
 
    // create the property
    if($property->create()){
 
        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode(array("message" => "Propiedad Agregada exitosamente!."));
    }
 
    // if unable to create the property, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Error al agregar la propiedad."));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Error al agregar la propiedad, informacion incompleta."));
}
?>