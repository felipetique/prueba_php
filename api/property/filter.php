<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// initialize object
$data = json_decode(file_get_contents("../assets/data-1.json"), true);
foreach ($data as $key => $value) {
    $data[$key]['PrecioInt'] = str_replace(array("$",","),"", $value['Precio']);
}
$queries = array();
parse_str($_SERVER['QUERY_STRING'], $queries);
if (array_key_exists('PrecioInt',$queries)) {
    $queries['PrecioInt'] = explode(';', $queries['PrecioInt']);
}
$new = array();
foreach ($queries as $key => $value) {
    if (count($new) === 0) {
        $new = array_filter($data, function ($var) use ($key,$value) {
            if ($key === 'PrecioInt') {
                return ($var["$key"] >= $value[0] && $var["$key"] <= $value[1]);
            } else {
                return ($var["$key"] == $value);
            }
        });
    } else {
        $new = array_filter($new, function ($var) use ($key,$value) {
            if ($key === 'PrecioInt') {
                return ($var["$key"] >= $value[0] && $var["$key"] <= $value[1]);
            } else {
                return ($var["$key"] == $value);
            }
        });
    }
}
// set response code - 200 OK
http_response_code(200);

// show products data in json format
echo json_encode($new);
//$product = new Product()