<?php
$filename = "Exportardo".date('Ymdhhssmm').".xls";

header("Content-Type: application/vnd.ms-excel");
header('Content-Disposition: attachment; filename="'.$filename.'"');

// include database and object files
include_once '../config/database.php';
include_once '../objects/property.php';

// instantiate database and property object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$property = new Property($db);
$queries = array();
parse_str($_SERVER['QUERY_STRING'], $queries);
foreach ($queries as $key => $value) {
    $property->$key = $value;
}
// query propertys
$stmt = $property->read();
$num = $stmt->rowCount();
// check if more than 0 record found
if($num>0){
  
    // propertys array
    $propertys_arr=array();
    $propertys_arr["records"]=array();
  
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
  
        $property_item=array(
            "id" => $id,
            "ciudad" => $ciudad,
            "direccion" => $direccion,
            "telefono" => $telefono,
            "codigo_postal" => $codigo_postal,
            "tipo" => $tipo,
            "precio" => $precio
        );
  
        array_push($propertys_arr["records"], $property_item);
    }
    $show_coloumn = false;
    if(!empty($propertys_arr["records"])) {
        foreach($propertys_arr["records"] as $record) {
            if(!$show_coloumn) {
                // display field/column names in first row
                echo implode("\t", array_keys($record)) . "\n";
                $show_coloumn = true;
            }
            echo implode("\t", array_values($record)) . "\n";
        }
    }
}

exit;