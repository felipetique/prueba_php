<?php
class Property{
   
    // database connection and table name
    private $conn;
    private $table_name = "property";

    // object properties
    public $id;
    public $ciudad;
    public $direccion;
    public $telefono;
    public $codigo_postal;
    public $tipo;
    public $precio;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // create property
    function create(){
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    ciudad=:ciudad,direccion=:direccion, telefono=:telefono, codigo_postal=:codigo_postal, tipo=:tipo, precio=:precio";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->ciudad=htmlspecialchars(strip_tags($this->ciudad));
        $this->direccion=htmlspecialchars(strip_tags($this->direccion));
        $this->telefono=htmlspecialchars(strip_tags($this->telefono));
        $this->codigo_postal=htmlspecialchars(strip_tags($this->codigo_postal));
        $this->tipo=htmlspecialchars(strip_tags($this->tipo));
        $this->precio=htmlspecialchars(strip_tags($this->precio));
    
        // bind values
        $stmt->bindParam(":ciudad", $this->ciudad);
        $stmt->bindParam(":direccion", $this->direccion);
        $stmt->bindParam(":telefono", $this->telefono);
        $stmt->bindParam(":codigo_postal", $this->codigo_postal);
        $stmt->bindParam(":tipo", $this->tipo);
        $stmt->bindParam(":precio", $this->precio);
    
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;  
    }
    // delete property
    function delete() {
        $query = "DELETE FROM " . $this->table_name . " WHERE id=:id";
        // prepare query
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":id", $this->id);
        
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false; 
    }
    // read property
    function read(){
        $q = '';
        // select all query
        if ($this->ciudad) {
            if ($q === '') {
                $q .= "WHERE ciudad='{$this->ciudad}'";
            } else {
                $q .= " AND ciudad='{$this->ciudad}'";
            }
        }
        
        if ($this->tipo) {
            if ($q === '') {
                $q .= "WHERE tipo='{$this->tipo}'";
            } else {
                $q .= " AND tipo='{$this->tipo}'";
            }
        }
        
        $query = "SELECT * FROM property $q ";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        
        // execute query
        $stmt->execute();
    
        return $stmt;
    }
    // export property

}
?>