<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// initialize object
$data = json_decode(file_get_contents("../assets/data-1.json"), true);
$citys = array();
$types = array();

foreach ($data as $value) {
    array_push($citys,$value["Ciudad"]);
    array_push($types,$value["Tipo"]);
}

$citys = array_unique($citys);
$types = array_unique($types);

// set response code - 200 OK
http_response_code(200);

// show products data in json format
echo json_encode(array("ciudades" => $citys,"tipos" => $types));
//$product = new Product()